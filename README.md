In Application :: 

Install

1.mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=org.utt.app -DartifactId=InApp<br>
2.cd InApp<br>
3.mvn compile<br>
4.mvn eclipse:eclipse<br>
5.To import this project in eclipse choose: File->Import->Maven->Existing Maven Projects. Then select the folder where your project has been created.<br>


Deploying

<build>
  <plugins>
    <plugin>
      <artifactId>maven-assembly-plugin</artifactId>
      <configuration>
        <archive>
          <manifest>
            <mainClass>org.utt.app.InApp</mainClass>
          </manifest>
        </archive>
        <descriptorRefs>
          <descriptorRef>jar-with-dependencies</descriptorRef>
        </descriptorRefs>
      </configuration>
    </plugin>
  </plugins>
</build>


mvn compile assembly:single