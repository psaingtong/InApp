package org.utt.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class NorthPanel extends JPanel {
	Dimension screen;

	public NorthPanel() {
		setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(new Dimension(screen.width, 60));
	}

}
