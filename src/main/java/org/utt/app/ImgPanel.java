package org.utt.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import org.openimaj.image.FImage;
import org.openimaj.image.ImageUtilities;
import org.openimaj.image.MBFImage;
import org.openimaj.image.colour.RGBColour;
import org.openimaj.image.colour.Transforms;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.FaceDetector;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;
import org.openimaj.image.processing.face.detection.keypoints.FKEFaceDetector;
import org.openimaj.image.processing.face.detection.keypoints.FacialKeypoint;
import org.openimaj.image.processing.face.detection.keypoints.KEDetectedFace;
import org.openimaj.math.geometry.point.Point2d;
import org.openimaj.math.geometry.point.Point2dImpl;
import org.openimaj.math.geometry.shape.Shape;
import org.openimaj.video.Video;
import org.openimaj.video.VideoDisplay;
import org.openimaj.video.VideoDisplayListener;
import org.openimaj.video.capture.Device;
import org.openimaj.video.capture.VideoCapture;
import org.openimaj.video.capture.VideoCaptureException;

public class ImgPanel extends JPanel {

	Dimension screen;
	JPanel videoDisplayPanel,topPanel;
	JLabel labelVideo;
	JButton ButtonTS;
	Video<MBFImage> video;
	VideoDisplay display;
	
	public ImgPanel() {
		setBorder(new TitledBorder(null, "Image Processing", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		screen = Toolkit.getDefaultToolkit().getScreenSize();
		setLayout(new BorderLayout(0, 0));
		setPreferredSize(new Dimension(screen.width, 600));
		
		topPanel = new JPanel();
		topPanel.setLayout(null);
		topPanel.setPreferredSize(new Dimension(680, 40));
		add(topPanel,BorderLayout.NORTH);
		
		ButtonTS = new JButton("Take Snapshot");
		ButtonTS.setBounds((screen.width/2)-75, 10, 150, 25);
		ButtonTS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				display.setMode(VideoDisplay.Mode.PAUSE);
				final JFileChooser saveFile = new JFileChooser();
				if (saveFile.showSaveDialog(videoDisplayPanel) == JFileChooser.APPROVE_OPTION) {
					File outfile = saveFile.getSelectedFile();
					outfile = new File(outfile.getParentFile(), outfile.getName() + ".jpg");
					try {
						ImageUtilities.write(video.getCurrentFrame(), outfile);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						JOptionPane.showMessageDialog(null, "Unable to save file.");
					}
				}
				display.setMode(VideoDisplay.Mode.PLAY);
			}
			
		});
		topPanel.add(ButtonTS);
		List<Device> videoDevices = VideoCapture.getVideoDevices();
        System.out.println(videoDevices.size() + " video devices detected.");
       
        try {
			video = new VideoCapture(680, 480, 24, videoDevices.get(0));
			//VideoDisplay<MBFImage> display = VideoDisplay.createVideoDisplay(video);
			videoDisplayPanel=new JPanel();
			videoDisplayPanel.setPreferredSize(new Dimension(680, 480));
			videoDisplayPanel.setBounds(0,30, 680, 480);
			videoDisplayPanel.setOpaque(false);
			display=VideoDisplay.createVideoDisplay(video,videoDisplayPanel);
			display.addVideoListener(new VideoDisplayListener<MBFImage>() {
	            //FaceDetector<DetectedFace, FImage> fd = new HaarCascadeDetector(100);
				FaceDetector<KEDetectedFace, FImage> fd = new FKEFaceDetector();
	            public void beforeUpdate(MBFImage frame) {
	            	/*
	                List<DetectedFace> faces = fd.detectFaces(Transforms.calculateIntensity(frame));
	                for (DetectedFace face : faces)
	                    frame.drawShape(face.getBounds(), RGBColour.RED);
	                 */
	            	 List<KEDetectedFace> faces = fd.detectFaces(Transforms.calculateIntensity(frame));
	                 	for (KEDetectedFace face : faces) {
	                 		Shape shape = face.getShape();
	                        frame.drawShape(shape, RGBColour.RED);
	                        Point2d faceOrigin = new Point2dImpl(shape.minX(), shape.minY());
	                        FacialKeypoint[] keypoints = face.getKeypoints();
	                        for (FacialKeypoint point : keypoints) {
	                            point.position.translate(faceOrigin);
	                            frame.drawPoint(point.position, RGBColour.WHITE, 10);
	                        }
	                 	}
	                /*
	                BufferedImage img = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
                    Graphics2D g = (Graphics2D) img.getGraphics();
                    g.drawImage(ImageUtilities.createBufferedImageForDisplay(frame), 0, 0, 680, 480, null);
                    */
                 //here is the label that I use to display the video
                    //labelVideo.setIcon(new ImageIcon(ImageUtilities.createBufferedImageForDisplay(frame)));
	            }
	            public void afterUpdate(VideoDisplay<MBFImage> display) {
	            	
	            }
			 });
			
		} catch (VideoCaptureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
        add(videoDisplayPanel,BorderLayout.CENTER);
         
		

	}

}
