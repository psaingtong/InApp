package org.utt.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;

import javax.swing.JInternalFrame;

public class ImgFrame extends JInternalFrame {
	Dimension screen;
	ImgPanel imgPanel;

	public ImgFrame() {
		screen = Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(0, 0, screen.width, screen.height-80);
        setPreferredSize(new Dimension(screen.width, screen.height-80));
		setTitle("frame1.title");
	    setLocation(0, 30);

	    setClosable(true);
	    setIconifiable(true);
	    setMaximizable(true);
	    
	    setResizable(true);
	    setDefaultCloseOperation(HIDE_ON_CLOSE);
	    
	    getContentPane().setLayout(new BorderLayout(0, 0));
	    imgPanel = new ImgPanel();
	    getContentPane().add(imgPanel, BorderLayout.CENTER);
	    setVisible(false);

	}

}
