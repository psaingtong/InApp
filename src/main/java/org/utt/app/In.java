package org.utt.app;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.UIManager;

import org.utt.app.dao.DAOInit;
import org.utt.app.util.Prop;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.GrayColor;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfGState;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.RandomAccessFileOrArray;
import com.lowagie.text.pdf.codec.TiffImage;

/**
 * Hello world!
 *
 */
public class In {
	
	public In() {
		 
		//tifftopdf();
		//tifftojpg();
		new InApp();
		
	}
	public void tifftopdf() {
		RandomAccessFileOrArray ra;
		float x, y;
		try {
			ra = new RandomAccessFileOrArray("/home/putty/Documents/sample/222.tif");
			int n = TiffImage.getNumberOfPages(ra);
			com.lowagie.text.Image image = TiffImage.getTiffImage(ra, 1);
	    	com.lowagie.text.Rectangle pageSize = new com.lowagie.text.Rectangle(image.getWidth(), image.getHeight()+80);
	    	Document document = new Document(pageSize);
			System.out.println("total page.."+n+"img size"+image.getWidth()+":"+image.getHeight()+"page.."+pageSize.getWidth()+":"+pageSize.getHeight());
			
			PdfWriter writer = PdfWriter.getInstance(document,  new FileOutputStream("/home/putty/Documents/sample/222.pdf"));
	    	
	    	writer.setStrictImageSequence(true);
	    	document.open();
	    	document.add(image);
	    	
	    	Phrase p1 = new Phrase("123456789012345",FontFactory.getFont(FontFactory.COURIER, 24, com.lowagie.text.Font.BOLD,new GrayColor(0.5f)));
	    	PdfContentByte canvas = writer.getDirectContent();
	    	x = (pageSize.getLeft() + pageSize.getRight()) / 4;
	    	y = (pageSize.getTop() + pageSize.getBottom()) / 10;
	    	canvas.saveState();
	    	//document.newPage();
	    	PdfGState state = new PdfGState();
	    	state.setFillOpacity(0.2f);
	        canvas.setGState(state);
	        ColumnText.showTextAligned(canvas, Element.ALIGN_CENTER, p1, x, y, 0);
	        canvas.restoreState();
	        document.close();
	        ra.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
	public void tifftojpg() {
		BufferedImage tif;
		try {
			tif = ImageIO.read(new File("/home/putty/Documents/sample/123456.tif"));
			ImageIO.write(tif, "png", new File("/home/putty/Documents/sample/112233.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	}
    public static void main( String[] args ){
    	try {
    		String name = UIManager.getSystemLookAndFeelClassName();
    		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			//UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			UIManager.put("PopupMenu.consumeEventOnClose", Boolean.TRUE);
			UIManager.put("OptionPane.background",new Color(243, 223, 208));
			UIManager.put("Dialog.background",new Color(243, 223, 208));
			UIManager.put("Panel.background",new Color(243, 223, 208));
			UIManager.put("InternalFrame.activeTitleBackground", Color.red);
	        UIManager.put("InternalFrame.activeTitleForeground", Color.blue);
	        UIManager.put("InternalFrame.inactiveTitleBackground", Color.black);
	        UIManager.put("InternalFrame.inactiveTitleForeground", Color.yellow);
			//UIManager.put("TitledBorder.border", new LineBorder(new Color(200,200,200), 1));
			 
			//setUIFont (new javax.swing.plaf.FontUIResource(THSarabunNew));
	        //System.out.println(name);
		} catch (Exception e) { }
    	EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Prop.init();
					DAOInit.init();
					macosConfig();
			        new In();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
         
    }
    public static void macosConfig() {
    	System.setProperty("apple.laf.useScreenMenuBar", "true");
    }
}
