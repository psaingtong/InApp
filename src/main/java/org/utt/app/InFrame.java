package org.utt.app;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import java.awt.Insets;
import java.awt.Toolkit;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JInternalFrame;

public class InFrame extends JInternalFrame {

	Dimension screen;
	NorthPanel northPanel;
	
	public InFrame() {
 
		screen = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds(0, 0, screen.width, screen.height-80);
        setPreferredSize(new Dimension(screen.width, screen.height-80));
		setTitle("frame1.title");
	    setLocation(0, 30);

	    setClosable(true);
	    setIconifiable(true);
	    setMaximizable(true);
	    
	    setResizable(true);
	    setDefaultCloseOperation(HIDE_ON_CLOSE);
	    
	    getContentPane().setLayout(new BorderLayout(0, 0));
	    
	    northPanel = new NorthPanel();
	    getContentPane().add(northPanel, BorderLayout.NORTH);


	    setVisible(false);
	    
	    

	}

}
